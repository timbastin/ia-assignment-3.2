import { Express } from "express";
import { ControllerClass } from "./types";
import EvaluationRecordController from "./controller/EvaluationRecordController";
import DbAccess from "./db/DbAccess";
import { Connection } from "typeorm";
import bodyParser from "body-parser";
import { NextHandleFunction } from "connect";

export default class App {
    static controller: ControllerClass<any, any>[] = [EvaluationRecordController];
    static middleware: NextHandleFunction[] = [bodyParser.json()];
    static connection: Connection;
    static readonly httpMethods = ["get", "post", "delete", "put"];

    static async boot(express: Express) {
        await this.initializeDatabase();
        await this.initializeMiddleware(express);
        this.controller.forEach((controller) => {
            const instance = this.initiate(controller);
            const basePath = Reflect.getMetadata("app:path", controller);
            if (!basePath) {
                throw new Error(controller.name + " not decorated with the CrudController decorator");
            }
            this.httpMethods.forEach((method) => {
                const methodHandler = Reflect.getMetadata("app:" + method, instance);
                if (methodHandler) {
                    const methodPath = Reflect.getMetadata("app:" + method + ":path", instance);
                    const completePath = basePath + methodPath;
                    // @ts-ignore
                    express[method](completePath, instance[methodHandler]);
                    console.log(
                        "Registered: " +
                            controller.name +
                            "@" +
                            instance[methodHandler].name +
                            " to path: " +
                            method.toUpperCase() +
                            " " +
                            completePath,
                    );
                }
            });
        });
        await express.listen(3000, () => console.log("Application started successfully."));
    }

    private static async initializeMiddleware(express: Express) {
        this.middleware.forEach((fn) => express.use(fn));
    }

    private static async initializeDatabase() {
        this.connection = await DbAccess.init();
    }

    // in the future this can be improved to even inject dynamic dependencies.
    // then it might be quite cool to have a public method on the DI container.
    public static initiate(clazz: ControllerClass<any, any>) {
        // pretty good place to do some dependency injection.
        // lets inspect the metadata to identify the correct repository for the controller class and just inject it.
        const modelType = Reflect.getMetadata("app:model", clazz);
        return new clazz(this.connection.getRepository(modelType));
    }
}

import { Connection, createConnection } from "typeorm";
import EvaluationRecord from "../models/EvaluationRecord";

/**
 * Does handle the connection to a database.
 */
export default class DbAccess {
    private static readonly cred = {
        type: "mongodb" as const,
        useUnifiedTopology: true,
        host: "localhost",
        port: 27017,
        database: "ia",
        entities: [EvaluationRecord],
    };

    private static connection: Connection;

    public static async init() {
        if (!this.connection) {
            this.connection = await createConnection(DbAccess.cred);
        }
        return this.connection;
    }
}

import express from "express";
import App from "./App";

const app = express();

App.boot(app);

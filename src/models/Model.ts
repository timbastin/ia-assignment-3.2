import { CreateDateColumn, ObjectID, ObjectIdColumn, UpdateDateColumn } from "typeorm";

export default abstract class Model {
    @ObjectIdColumn()
    id: ObjectID;

    @UpdateDateColumn()
    updatedAt: Date;

    @CreateDateColumn()
    createdAt: Date;
}

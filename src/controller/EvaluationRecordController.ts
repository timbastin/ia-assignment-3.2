import { Controller, DELETE, GET, POST, PUT } from "./Controller";
import e from "express";
import EvaluationRecord from "../models/EvaluationRecord";
import { Repository } from "typeorm";

@Controller("/evaluation-records", EvaluationRecord)
export default class EvaluationRecordController {
    constructor(protected readonly employeeRepository: Repository<EvaluationRecord>) {}

    @DELETE()
    delete(req: e.Request, res: e.Response): void {}

    @GET()
    get(req: e.Request, res: e.Response): void {}

    @POST()
    post(req: e.Request, res: e.Response): void {}

    @PUT()
    put(req: e.Request, res: e.Response): void {}
}

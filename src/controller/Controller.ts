import Model from "../models/Model";

export interface Clazz<T> {
    new (): T;
}

export const GET = (path = ""): MethodDecorator => (target: Clazz<any>, propertyKey: string) => {
    Reflect.defineMetadata("app:get", propertyKey, target);
    Reflect.defineMetadata("app:get:path", path, target);
};
export const POST = (path = ""): MethodDecorator => (target: Clazz<any>, propertyKey: string) => {
    Reflect.defineMetadata("app:post", propertyKey, target);
    Reflect.defineMetadata("app:post:path", path, target);
};
export const PUT = (path = ""): MethodDecorator => (target: Clazz<any>, propertyKey: string) => {
    Reflect.defineMetadata("app:put", propertyKey, target);
    Reflect.defineMetadata("app:put:path", path, target);
};
export const DELETE = (path = ""): MethodDecorator => (target: Clazz<any>, propertyKey: string) => {
    Reflect.defineMetadata("app:delete", propertyKey, target);
    Reflect.defineMetadata("app:delete:path", path, target);
};

export const Controller = <T extends Clazz<Model>>(path: string, model: T) => (target: Function) => {
    Reflect.defineMetadata("app:path", path, target);
    Reflect.defineMetadata("app:model", model, target);
};

import Model from "./models/Model";
import { Repository } from "typeorm";

export interface ControllerClass<T extends Model, Controller> {
    new (repository: Repository<T>): Controller;
}

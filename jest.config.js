module.exports = {
    moduleFileExtensions: ["js", "json", "ts"],
    testEnvironment: "node",
    transform: {
        "^.+\\.(t|j)s$": "ts-jest",
    },
    rootDir: ".",
    setupFiles: ["./jest-setup-file.ts"],
    testRegex: ".spec.ts$",
};
